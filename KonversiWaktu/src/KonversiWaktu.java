import java.util.Scanner;

public class KonversiWaktu {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputLagi;
        do {
            System.out.print("Masukkan jumlah detik : ");
            int detik = scanner.nextInt();
            scanner.nextLine();

            int hari = detik / 86400;
            detik = detik % 86400;
            int jam = detik / 3600;
            detik = detik % 3600;
            int menit = detik / 60;
            detik = detik % 60;

            System.out.println("Hari    : " + hari);
            System.out.println("Jam     : " + jam);
            System.out.println("Menit   : " + menit);
            System.out.println("Detik   : " + detik);

            System.out.print("Input data lagi [Y/T]? ");
            inputLagi = scanner.nextLine();
        } while (inputLagi.equalsIgnoreCase("Y"));
    }
}